/**
 * Created by AMINOVISH.B on 07/02/2015.
 */
var view = require('../api/personnesView'),
    _ = require('underscore');
var config = require('../config/' + process.env.NODE_ENV + '.js');
var bootstrap = require('../api/bootstrap');
var countName = "personne::count";
bootstrap.init(config);
var db = bootstrap.dbBucket;
exports.getPersonneById = function (req, resp) {
    db.get('personne_' + req.params.id, function (err, res) {
        if (res) {
            resp.send(res.value);
        } else {
            resp.send([]);
        }
    });
};
exports.createPersonne = function (req, resp) {
    var count;
    db.get(countName, function (err, res) {
        count = res.value;
        var personne = req.body;
        personne.meta={};
        personne.meta.docType = "personne";
        personne.meta.personneId = count;
        personne.meta.docId = "personne_" + personne.meta.personneId;
        console.log(personne.meta.docId);
        db.insert(personne.meta.docId, personne, function () {
            db.counter(countName, 1, function (err, responce) {
                if (err) throw err
                resp.send(responce);
            });

        });


    });


}
exports.updatePersonne = function (req, resp) {
    var personne = req.body;
    personne.meta={};
    personne.meta.publicationId = req.params.id;
    personne.meta.docType = "publication";
    personne.meta.docId = "personne_" + req.params.id;
    db.replace(personne.meta.docId, personne, function (err, res) {
        if (err) throw err
        resp.send(res);
    });
}
exports.deletePersonne = function (req, resp) {
    db.remove("personne_" + req.params.id, function (err, res) {
        if (err) throw err
        resp.send(res);
    });
}
exports.getAllPersonne = function (callback) {
    view.getAllPersonnesView(function (personnes) {
        if (personnes.length > 0) {
            db.getMulti(personnes, function (err, res) {
                if (err) throw err;
                var val = _.pluck(res, "value");
                callback(res);
            });
        } else {
            callback([]);
        }

    });
}
exports.getAllPartyPersonnes= function (callback) {
    view.getAllPartyPersonnesView(function (personnes) {
        if (personnes.length > 0) {
            db.getMulti(personnes, function (err, res) {
                if (err) throw err;
                var val = _.pluck(res, "value");
                callback(val);
            });
        } else {
            callback([]);
        }

    });
}
exports.getAllGovernementPersonnes= function (callback) {
    view.getAllGovernementPersonnesView(function (personnes) {
        if (personnes.length > 0) {
            db.getMulti(personnes, function (err, res) {
                if (err) throw err;
                var val = _.pluck(res, "value");
                callback(val);
            });
        } else {
            callback([]);
        }

    });
}
exports.getAllBlocPersonnes= function (callback) {
    view.getAllBlocPersonnesView(function (personnes) {
        if (personnes.length > 0) {
            db.getMulti(personnes, function (err, res) {
                if (err) throw err;
                var val = _.pluck(res, "value");
                callback(val);
            });
        } else {
            callback([]);
        }

    });
}
exports.getAllAssemblyPersonnes= function (callback) {
    view.getAllAssemblyPersonnesView(function (personnes) {
        if (personnes.length > 0) {
            db.getMulti(personnes, function (err, res) {
                if (err) throw err;
                var val = _.pluck(res, "value");
                callback(val);
            });
        } else {
            callback([]);
        }

    });
}