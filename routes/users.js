var view = require('../api/usersView.js'),
    _ = require('underscore');
var config = require('../config/' + process.env.NODE_ENV + '.js');
var bootstrap = require('../api/bootstrap');
var countName = "users::count";
bootstrap.init(config);
var db = bootstrap.dbBucket;

exports.getUserById = function (req, resp)
{
    db.get('user_' + req.params.id, function (err, res)
    {
        if(res){
            resp.send(res.value);
        }else{
            resp.send([]);
        }
    });
};
exports.createUser = function (req, resp)
{
    var count;
    db.get(countName, function (err, res)
    {
        count = res.value;
        var user = req.body;
        user.meta = {};
        user.meta.docType = "user";
        user.meta.userId = count;
        user.meta.docId = "user_" + user.meta.userId;
        db.insert(user.meta.docId, user, function ()
        {
            db.counter(countName, 1, function (err, responce)
            {
                if (err) throw err
                resp.send(responce);
            });

        });


    });


}
exports.updateUser = function (req, resp)
{
    var user = req.body;
    user.meta = {};
    user.meta.userId = req.params.id;
    user.meta.docType = "user";
    user.meta.docId = "user_" + req.params.id;
    db.replace(user.meta.docId, user, function (err, res)
    {
        if (err) throw err
        resp.send(res);
    });
}
exports.deleteUser = function (req, resp)
{
    db.remove("user_" + req.params.id, function (err, res)
    {
        if (err) throw err
        resp.send(res);
    });
}
exports.getAllUser = function (callback)
{
    view.getAllUserView(function (users)
    {
        if(users.length>0){
            db.getMulti(users, function (err, resp)
            {
                if (err) throw err;
                var val = _.pluck(resp, "value");
                callback(val);
            });
        }else{
            callback([]);
        }

    });
}
exports.getWebadminUsers = function (callback)
{
    view.getWebadminUsersView(function (users)
    {
        if(users.length>0){
            console.log(users);
            db.getMulti(users, function (err, resp)
            {
                if (err) throw err;
                var val = _.pluck(resp, "value");
                callback(val);
            });
        }else{
            callback([]);
        }
    });
}