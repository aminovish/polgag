/**
 * Created by AMINOVISH.B on 07/02/2015.
 */
var view = require('../api/publicationView'),
    _ = require('underscore');
var config = require('../config/' + process.env.NODE_ENV + '.js');
var bootstrap = require('../api/bootstrap');
var countName = "publication::count";
bootstrap.init(config);
var db = bootstrap.dbBucket;
exports.getPublicationById = function (req, resp) {
    db.get('publication_' + req.params.id, function (err, res) {
        if (res) {
            resp.send(res.value);
        } else {
            resp.send([]);
        }
    });
};
exports.createPublication = function (req, resp) {
    var count;
    db.get(countName, function (err, res) {
        count = res.value;
        var publication = req.body;
        publication.meta.docType = "publication";
        publication.meta.publicationId = count;
        publication.meta.docId = "publication_" + publication.meta.publicationId;
        console.log(publication.meta.docId);
        db.insert(publication.meta.docId, publication, function () {
            db.counter(countName, 1, function (err, responce) {
                if (err) throw err
                resp.send(responce);
            });

        });


    });


}
exports.updatePublication = function (req, resp) {
    var publication = req.body;
    publication.meta.publicationId = req.params.id;
    publication.meta.docType = "publication";
    publication.meta.docId = "publication_" + req.params.id;
    db.replace(publication.meta.docId, publication, function (err, res) {
        if (err) throw err
        resp.send(res);
    });
}
exports.deletePublication = function (req, resp) {
    db.remove("publication_" + req.params.id, function (err, res) {
        if (err) throw err
        resp.send(res);
    });
}
exports.getAllPublication = function (req, resp) {
    view.getAllPublicationView(function (publications) {
        if (publications.length > 0) {
            db.getMulti(publications, function (err, res) {
                if (err) throw err;
                var val = _.pluck(res, "value");
                console.log(val);
                resp.send(val);
            });
        } else {
            resp([]);
        }

    });
}