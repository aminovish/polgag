/**
 * Created by AMINOVISH.B on 07/02/2015.
 */

var config = require('../config/' + process.env.NODE_ENV + '.js');
var bootstrap = require('../api/bootstrap'),
    _ = require('underscore');
var couchbase = require("couchbase");
bootstrap.init(config);
var db = bootstrap.dbBucket;
module.exports =
{
    getAllPersonnesView: function (callback)
    {
        var ViewQuery = couchbase.ViewQuery;
        var query = ViewQuery.from("dev_personnes", 'allPersonnes');
        db.query(query, function (err, resp)
        {
            if (err) throw err;
            var results = _.map(resp, function (num)
            {
                return num.id;
            });
            callback(results);

        });
    },
    getAllPartyPersonnesView: function (callback)
    {
        var ViewQuery = couchbase.ViewQuery;
        var query = ViewQuery.from("dev_personnes", 'allPartyPersonnes');
        db.query(query, function (err, resp)
        {
            if (err) throw err;
            var results = _.map(resp, function (num)
            {
                return num.id;
            });
            callback(results);

        });
    },
    getAllGovernementPersonnesView: function (callback)
    {
        var ViewQuery = couchbase.ViewQuery;
        var query = ViewQuery.from("dev_personnes", 'allGovernementPersonnes');
        db.query(query, function (err, resp)
        {
            if (err) throw err;
            var results = _.map(resp, function (num)
            {
                return num.id;
            });
            callback(results);

        });
    },
    getAllBlocPersonnesView: function (callback)
    {
        var ViewQuery = couchbase.ViewQuery;
        var query = ViewQuery.from("dev_personnes", 'allBlocPersonnes');
        db.query(query, function (err, resp)
        {
            if (err) throw err;
            var results = _.map(resp, function (num)
            {
                return num.id;
            });
            callback(results);

        });
    },
    getAllAssemblyPersonnesView: function (callback)
    {
        var ViewQuery = couchbase.ViewQuery;
        var query = ViewQuery.from("dev_personnes", 'allAssemblyPersonnes');
        db.query(query, function (err, resp)
        {
            if (err) throw err;
            var results = _.map(resp, function (num)
            {
                return num.id;
            });
            callback(results);

        });
    }
}
