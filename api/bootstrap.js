/**
 * Created by AMINOVISH.B on 07/02/2015.
 */
var couchbase = require("couchbase");
module.exports =
{
    init: function (config)
    {
        var myCluster = new couchbase.Cluster(config.db.host + ":" + config.db.port);
        this.dbBucket = myCluster.openBucket(config.db.bucket, function ()
        {
        });
    }
}