"use strict";
var express = require('express'),
    users = require('./routes/users'),
    personnes = require('./routes/personnes'),
    publications = require('./routes/publication'),
    bodyParser = require('body-parser'),
    config = require('./config/' + process.env.NODE_ENV + '.js'),
    _ = require('underscore');

var app = express();
// parse application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({ extended: false }))

// parse application/json
app.use(bodyParser.json())
/*
 Users Restful API
 */
app.get('/users/:id', users.getUserById);
app.get('/users', function (req, res, next) {
    var functionMapping = {
        webadmin: users.getWebadminUsers
    }
    if (_.has(req.query, "type")) {
        var func = _.find(functionMapping, function (v, k) {
            if (k == req.query.type) return v;
        });
        if (!_.isUndefined(func)) {
            func(function (user) {
                res.send(user);
            });
        } else {
            next();
        }
    } else {
        users.getAllUser(function (user) {
            res.send(user);
        });
    }
});
app.post('/users', users.createUser);
app.put("/users/:id", users.updateUser);
app.delete("/users/:id", users.deleteUser);
/*
 Publication Restful API
 */
app.get('/publications', publications.getAllPublication);
app.get('/publications/:id', publications.getPublicationById);
app.post('/publications', publications.createPublication);
app.put('/publications/:id', publications.updatePublication);
app.delete('/publications/:id', publications.deletePublication);
/*
 Personne Restful API
 */
app.get('/personnes', function (req, res, next) {
    var functionMapping = {
        assembly: personnes.getAllAssemblyPersonnes,
        government: personnes.getAllGovernementPersonnes
    }
    console.log("work");
    if (_.has(req.query, "type")) {
        var func = _.find(functionMapping, function (v, k) {
            if (k == req.query.type) return v;
        });
        if (!_.isUndefined(func)) {
            func(function (user) {
                res.send(user);
            });
        } else {
            next();
        }
    } else if (_.has(req.query, "party")) {
        personnes.getAllPartyPersonnes(function (listPersonnes) {
            console.log("test");
            _.each(listPersonnes, function (num) {
                console.log("key" + num);
                if (!_.isUndefined(num.partie) ) {
                    console.log("is equal 1");
                    if (_.isEqual(num.partie, req.query.party)) {
                        console.log("is equal 2");
                        res.send(num)
                    }
                    else {
                        next();
                    }
                } else {
                    next();
                }
            });
        });

    } else if (_.has(req.query, "bloc")) {
        personnes.getAllPartyPersonnes(function (listPersonnes) {

            _.each(listPersonnes, function (num) {
                 if (!_.isUndefined(num.bloc) ) {
                    if (_.isEqual(num.bloc, req.query.bloc)) {
                        res.send(num)
                    }
                    else {
                        next();
                    }
                } else {
                    next();
                }
            });
        });

    } else {
        personnes.getAllPersonne(function (personne) {
            res.send(personne);
        });
    }
});
app.get('/personnes/:id', personnes.getPersonneById);
app.post('/personnes', personnes.createPersonne);
app.put('/personnes/:id', personnes.updatePersonne);
app.delete('/personnes/:id', personnes.deletePersonne);
app.listen(config.appConfig.port);

module.exports = app;